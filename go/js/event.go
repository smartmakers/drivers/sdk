package js

import (
	"encoding/json"

	"github.com/robertkrimen/otto"
	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

func newEngineForUplinkEvent(evt lvl3.UplinkEvent) (*otto.Otto, error) {
	eng, err := newEngine(evt.Event)
	if err != nil {
		return nil, err
	}

	if err := eng.Set("payload", []byte(evt.Payload)); err != nil {
		return nil, err
	}

	if err := eng.Set("port", evt.Port); err != nil {
		return nil, err
	}

	return eng, nil
}

func newEngineForConfigEvent(evt lvl3.ConfigEvent) (*otto.Otto, error) {
	return newEngine(evt.Event)
}

func newEngineForActionEvent(evt lvl3.ActionEvent) (*otto.Otto, error) {
	eng, err := newEngine(evt.Event)
	if err != nil {
		return nil, err
	}

	// need to do some type conversion here to make this work with
	// the javascript engine
	actionList := make([]map[string]interface{}, len(evt.Actions))
	for idx := range evt.Actions {
		action := make(map[string]interface{})
		for k, v := range evt.Actions[idx] {
			action[string(k)] = string(v)
		}

		actionList = append(actionList, action)
	}

	if err := eng.Set("actions", actionList); err != nil {
		return nil, err
	}

	return eng, nil
}

func newEngine(evt lvl3.Event) (*otto.Otto, error) {
	eng := otto.New()

	cfg := make(map[string]interface{})
	if evt.Config != nil {
		if err := json.Unmarshal(evt.Config, &cfg); err != nil {
			return nil, err
		}
	}

	if err := eng.Set("config", cfg); err != nil {
		return nil, err
	}

	state := make(map[string]interface{})
	if evt.State != nil {
		if err := json.Unmarshal(evt.State, &state); err != nil {
			return nil, err
		}
	}

	if err := eng.Set("state", state); err != nil {
		return nil, err
	}

	if err := eng.Set("time", evt.Time.Unix()); err != nil {
		return nil, err
	}

	return eng, nil
}
