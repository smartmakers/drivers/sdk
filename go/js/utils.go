package js

import (
	"github.com/robertkrimen/otto"
	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

func AddUtilsCallbacks(vm *otto.Otto) error {
	return vm.Set("debug", func(call otto.FunctionCall) otto.Value {
		if len(call.ArgumentList) < 1 {
			return otto.UndefinedValue()
		}

		msg, err := call.Argument(0).ToString()
		if err != nil {
			return otto.UndefinedValue()
		}

		lvl3.Debug(msg)

		return otto.UndefinedValue()
	})
}

func convertValue(value otto.Value, f func([]byte) interface{}, size int) otto.Value {
	exp, err := value.Export()
	if err != nil {
		return otto.UndefinedValue()
	}

	if u, ok := exp.(uint8); ok {
		exp = []byte{u}
	}

	bytes, ok := exp.([]byte)
	if !ok {
		return otto.UndefinedValue()
	}

	if len(bytes) < size {
		return otto.UndefinedValue()
	}

	tmp := f(bytes)
	v, err := otto.ToValue(tmp)
	if err != nil {
		return otto.UndefinedValue()
	}

	return v
}

func convertArgument(call otto.FunctionCall, size int, f func([]byte) interface{}) otto.Value {
	if len(call.ArgumentList) < 1 {
		return otto.UndefinedValue()
	}

	return convertValue(call.Argument(0), f, size)
}
