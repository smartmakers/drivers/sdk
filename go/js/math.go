package js

import (
	"math"

	"github.com/robertkrimen/otto"
)

func AddMathCallbacks(vm *otto.Otto) error {
	return vm.Set("round", func(call otto.FunctionCall) otto.Value {
		if len(call.ArgumentList) < 1 {
			return otto.UndefinedValue()
		}

		num, err := call.Argument(0).ToFloat()
		if err != nil {
			return otto.UndefinedValue()
		}

		var digits int64
		if len(call.ArgumentList) < 2 {
			digits = 0
		} else {
			var err error
			digits, err = call.Argument(1).ToInteger()
			if err != nil {
				return otto.UndefinedValue()
			}
		}

		multiplier := math.Pow(10, float64(digits))
		rounded := math.Round(num*multiplier) / multiplier
		val, err := vm.ToValue(rounded)
		if err != nil {
			return otto.UndefinedValue()
		}

		return val
	})
}
