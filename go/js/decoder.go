package js

import (
	_ "embed"
	"os"
	"reflect"

	"github.com/pkg/errors"
	"github.com/robertkrimen/otto"
	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

// RunTTNv2Decoder runs the given javascript in TTNv2's supported style.
//
// This style expects that a function called "decode" is defined and that this function
// takes two parameters: the payload and the port.
// The value returned by the function is interpreted used as the decoded payload.
//
// Note that TTNv3 has a different decoder interface, so TTNv3 scripts will not work
// unmodified with this function.
func RunTTNv2Decoder(script []byte) {
	lvl3.Run(&decoder{
		script: append(script, []byte(";\ndecode(payload, port)")...),
	}, os.Args)
}

/* TODO: to be implemented
// RunLoriotDecoder runs the given javascript in Loriot's supported style.
//
// The payload is made available as a hex string in a variable called 'data',
// as an array of strings each containing the hexadecimal representation as one byte
// in the variable `p`, and a ss an array of numbers in the variable `v`.
//
// Futhermore, there's an object called `msg` which contains additional metadata about
// the message: `msg.EUI` contains the device EUI, `msg.fcnt` contains the
// frame counter, `msg.port` contains the port number, and `msg.ts` contains the time stamp.
// The scripts last line should be the result of the decoding.
func RunLoriotDecoder(script []byte) {}
*/

// RunDecoder runs the given javascript in the most simple way.
//
// Payload and port are pre-defined variables which can be used anywhere in the script.
// The payload is an a array of numbers, where each number represents one byte.
// The port is passed in as a number.
// The result of the last statement in the script must contain the result of the decoding.
func RunDecoder(script []byte) {
	lvl3.Run(&decoder{
		script: script,
	}, os.Args)
}

type decoder struct {
	lvl3.DefaultDriver
	script []byte
}

func (dec *decoder) Uplink(evt lvl3.UplinkEvent) (*lvl3.Response, error) {
	upd, err := decode(dec.script, evt.Payload, evt.Port)
	if err != nil {
		return nil, err
	}

	resp := lvl3.Response{
		State: map[string]interface{}{},
		Updates: []lvl3.Update{
			{
				Time:       evt.Time,
				Properties: upd,
			},
		},
	}

	return &resp, nil
}

func decode(script []byte, payload []byte, port int) (map[string]interface{}, error) {
	vm := otto.New()
	vm.Set("payload", payload)
	vm.Set("port", port)

	val, err := vm.Run(script)
	if err != nil {
		return nil, errors.Wrap(err, "running script")
	}

	obj, err := val.Export()
	if err != nil {
		return nil, errors.Wrap(err, "extracting script results")
	}

	// we detect two explicit error values:
	// 1. a JSON object that contains an error field
	// 2. just a string with the prefix error
	switch conv := obj.(type) {
	case map[string]interface{}:
		elem, ok := conv["error"]
		if ok {
			str, ok := elem.(string)
			if ok {
				return nil, errors.New(str)
			}
		}

		convertNumbersToFloat(conv)
		return conv, nil
	}

	return nil, errors.Errorf("%v is not a valid return type", reflect.TypeOf(obj))
}

func convertNumbersToFloat(m map[string]interface{}) {
	// We need to do this conversion because sometimes JSON Numbers are converted to integers,
	// sometimes to float64.
	for key, value := range m {
		switch v := value.(type) {
		case uint8:
			m[key] = float64(v)
		case uint16:
			m[key] = float64(v)
		case uint32:
			m[key] = float64(v)
		case uint64:
			m[key] = float64(v)
		case int8:
			m[key] = float64(v)
		case int16:
			m[key] = float64(v)
		case int32:
			m[key] = float64(v)
		case int64:
			m[key] = float64(v)
		}
	}
}
