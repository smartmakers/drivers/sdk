package js

import (
	"encoding/binary"
	"math"

	"github.com/robertkrimen/otto"
)

func AddTypeCallbacks(vm *otto.Otto) error {
	if err := vm.Set("uint8", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 1, func(b []byte) interface{} {
			return uint(b[0])
		})
	}); err != nil {
		return err
	}

	if err := vm.Set("int8", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 1, func(b []byte) interface{} {
			return int(b[0])
		})
	}); err != nil {
		return err
	}

	if err := vm.Set("uint16", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 2, func(b []byte) interface{} {
			return binary.BigEndian.Uint16(b)
		})
	}); err != nil {
		return err
	}
	if err := vm.Set("int16", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 2, func(b []byte) interface{} {
			return int16(binary.BigEndian.Uint16(b))
		})
	}); err != nil {
		return err
	}

	if err := vm.Set("uint32", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 4, func(b []byte) interface{} {
			return binary.BigEndian.Uint32(b)
		})
	}); err != nil {
		return err
	}

	if err := vm.Set("int32", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 4, func(b []byte) interface{} {
			return int32(binary.BigEndian.Uint32(b))
		})
	}); err != nil {
		return err
	}

	if err := vm.Set("float32", func(call otto.FunctionCall) otto.Value {
		return convertArgument(call, 4, func(b []byte) interface{} {
			return math.Float32frombits(binary.BigEndian.Uint32(b))
		})
	}); err != nil {
		return err
	}

	return nil
}
