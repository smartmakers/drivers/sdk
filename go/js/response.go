package js

import (
	"fmt"
	"reflect"
	"strconv"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

func convertResponse(evtTime time.Time, obj interface{}) (*lvl3.Response, error) {
	if str, ok := obj.(string); ok {
		return nil, errors.New(str)
	}
	response := lvl3.Response{}
	if objAsMap, ok := obj.(map[string]interface{}); ok {
		stateField := objAsMap["state"]
		if state, ok := stateField.(map[string]interface{}); ok {
			response.State = state
		} else if stateField != nil {
			return nil, errors.Errorf("state must be a map, was %s", reflect.TypeOf(stateField))
		}

		lvl3Downlinks, err := convertDownlinks(objAsMap["downlinks"])
		if err != nil {
			return nil, err
		}

		response.Downlinks = lvl3Downlinks

		updsField := objAsMap["updates"]
		if upds, ok := updsField.([]map[string]interface{}); ok {
			for _, upd := range upds {
				prop, ok := upd["properties"]
				if !ok {
					return nil, errors.Errorf("update must have properties field")
				}

				t := evtTime
				if v, ok := upd["time"]; ok {
					if i, ok := v.(int64); ok {
						t = time.Unix(i, 0).UTC()
					} else if i, ok := v.(uint64); ok {
						t = time.Unix(int64(i), 0).UTC()
					} else if i, ok := v.(float64); ok {
						t = time.Unix(int64(i), 0).UTC()
					} else {
						return nil, errors.Errorf("time must be int64, was %s", reflect.TypeOf(upd["time"]))
					}
				}

				u := lvl3.Update{
					Time:       t,
					Properties: prop,
				}

				response.Updates = append(response.Updates, u)
			}
		} else if updsField != nil {
			return nil, errors.Errorf("updates must be an array, was %s", reflect.TypeOf(updsField))
		}

		devHltField := objAsMap["device_health"]
		if healths, ok := devHltField.([]map[string]interface{}); ok {
			for _, hlt := range healths {
				prop, ok := hlt["properties"]
				if !ok {
					return nil, errors.Errorf("device_health must have properties field")
				}

				t := evtTime
				if v, ok := hlt["time"]; ok {
					if i, ok := v.(int64); ok {
						t = time.Unix(i, 0).UTC()
					} else if i, ok := v.(uint64); ok {
						t = time.Unix(int64(i), 0).UTC()
					} else if i, ok := v.(float64); ok {
						t = time.Unix(int64(i), 0).UTC()
					} else {
						return nil, errors.Errorf("time must be int64, was %s", reflect.TypeOf(hlt["time"]))
					}
				}

				response.AddDeviceHealth(t, prop)
			}
		} else if devHltField != nil {
			return nil, errors.Errorf("device_health must be an array, was %s", reflect.TypeOf(updsField))
		}
	}

	return &response, nil
}

func convertDownlinks(in interface{}) (lvl3.Downlinks, error) {
	if in == nil {
		return nil, nil
	}

	lvl3Downlinks := lvl3.Downlinks{}

	if dls, ok := in.([]map[string]interface{}); ok {
		for _, dl := range dls {
			downlink := lvl3.Downlink{}

			if pld, ok := dl["payload"].([]int8); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}
				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]uint8); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}
				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]int32); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}
				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]uint32); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}
				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]int64); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}
				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]uint64); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}
				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]float64); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}

				downlink.Payload = bytes

			} else if pld, ok := dl["payload"].([]float32); ok {
				var bytes []byte
				for _, b := range pld {
					bytes = append(bytes, byte(b))
				}

				downlink.Payload = bytes
			} else if pld, ok := dl["payload"].([]interface{}); ok {
				var bytes []byte
				for _, b := range pld {
					i, err := strconv.Atoi(fmt.Sprintf("%v", b))
					if err != nil {
						return nil, errors.Wrap(err, "converting downlink payload to byte array")
					}

					bytes = append(bytes, byte(i))
				}

				downlink.Payload = bytes
			} else {
				return nil, errors.Errorf("downlinks[*].payload must be a byte array, was %s", reflect.TypeOf(dl["payload"]))
			}

			if port, ok := dl["port"].(int64); ok {
				downlink.Port = uint8(port)
			} else if port, ok := dl["port"].(int); ok {
				downlink.Port = uint8(port)
			} else if port, ok := dl["port"].(float64); ok {
				downlink.Port = uint8(port)
			} else {
				return nil, errors.Errorf("downlinks[*].port must be an integer, was %s", reflect.TypeOf(dl["port"]))
			}

			confField := dl["confirmed"]
			if conf, ok := confField.(bool); ok {
				downlink.Confirmed = conf
			} else if confField != nil {
				return nil, errors.Errorf("downlink[*].confirmed flag must be a boolean, was %s", reflect.TypeOf(confField))
			}

			lvl3Downlinks = append(lvl3Downlinks, downlink)
		}
	} else if arr, ok := in.([]interface{}); ok && len(arr) == 0 {
		// do nothing
	} else {
		return nil, errors.Errorf("downlinks must be an array of objects, was %v", reflect.TypeOf(in))
	}

	return lvl3Downlinks, nil
}
