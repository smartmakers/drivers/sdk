package js

import (
	"os"

	"github.com/pkg/errors"
	"github.com/robertkrimen/otto"

	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

func RunDriver(script []byte) {
	lvl3.Run(&driver{
		script: script,
	}, os.Args)
}

// driver is a javascript-based driver
type driver struct {
	script []byte
	lvl3.DefaultDriver
}

func (drv *driver) Uplink(evt lvl3.UplinkEvent) (*lvl3.Response, error) {
	vm, err := newEngineForUplinkEvent(evt)
	if err != nil {
		return nil, errors.Wrap(err, "initializing javacript engine")
	}

	if err := AddCallbacks(vm); err != nil {
		return nil, err
	}

	val, err := vm.Run(append(drv.script, []byte("\nonUplink(config, state, payload, port, time)")...))
	if err != nil {
		return nil, errors.Wrap(err, "running script")
	}

	obj, err := val.Export()
	if err != nil {
		return nil, errors.Wrap(err, "extracting script results")
	}

	return convertResponse(evt.Time, obj)
}

func (drv *driver) Config(evt lvl3.ConfigEvent) (*lvl3.Response, error) {
	vm, err := newEngineForConfigEvent(evt)
	if err != nil {
		return nil, errors.Wrap(err, "initializing javacript engine")
	}

	if err := AddCallbacks(vm); err != nil {
		return nil, err
	}

	val, err := vm.Run(append(drv.script, []byte("\nonConfig(config, state)")...))
	if err != nil {
		return nil, errors.Wrap(err, "running script")
	}

	obj, err := val.Export()
	if err != nil {
		return nil, errors.Wrap(err, "extracting script results")
	}

	return convertResponse(evt.Time, obj)
}

func (drv *driver) Action(evt lvl3.ActionEvent) (*lvl3.Response, error) {
	vm, err := newEngineForActionEvent(evt)
	if err != nil {
		return nil, errors.Wrap(err, "initializing javacript engine")
	}

	if err := AddCallbacks(vm); err != nil {
		return nil, err
	}

	val, err := vm.Run(append(drv.script, []byte("\nonActions(config, state, actions)")...))
	if err != nil {
		return nil, errors.Wrap(err, "running script")
	}

	obj, err := val.Export()
	if err != nil {
		return nil, errors.Wrap(err, "extracting script results")
	}

	return convertResponse(evt.Time, obj)
}

func AddCallbacks(vm *otto.Otto) error {

	if err := AddMathCallbacks(vm); err != nil {
		return err
	}

	if err := AddTypeCallbacks(vm); err != nil {
		return err
	}

	if err := AddUtilsCallbacks(vm); err != nil {
		return err
	}

	return nil
}
