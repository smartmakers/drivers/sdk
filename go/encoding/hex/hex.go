package hex

import (
	"encoding/hex"
)

// Hex is a byte array represented as a hexadecimal string.
//
// This can be used to conveniently unmarshal/marshal data to hex strings.
//
// The type unmarshals and hex strings with and without the 0x prefix,
// but it always unmarshals without the prefix.
type Hex []byte

// MarshalText marshals the Hex object to an actual hex string
func (h Hex) MarshalText() ([]byte, error) {
	dst := make([]byte, hex.EncodedLen(len(h)))
	hex.Encode(dst, h)
	return dst, nil
}

// UnmarshalText unmarshal a hex object from a byte array
func (h *Hex) UnmarshalText(bytes []byte) error {
	if has0xPrefix(bytes) {
		bytes = bytes[2:]
	}

	dst := make([]byte, hex.DecodedLen(len(bytes)))
	_, err := hex.Decode(dst, bytes)
	*h = dst
	return err
}

func has0xPrefix(bytes []byte) bool {
	if len(bytes) < 2 {
		return false
	}

	if bytes[0] == '0' && bytes[1] == 'x' {
		return true
	}

	return false
}
