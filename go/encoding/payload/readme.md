# Payload Unmarshaling

This package allows unmarshaling binary payloads
into golang structs with minimal effort.

Many of golang's basic types are supported already and a
dedicated interface allows overwriting the decoding
for any other type.

## Example

The primary entrypoint of this package is the function
`Unmarshal`, which takes a pointer to an object and
a byte slice (`[]byte`).

The byte slice is unmarshal recursively into the given object.
For example, the binary payload `0xBE0B04000A`
can be unmarshaled into the following type:

    type Example struct {
        Header struct{
            MagicByte byte
            Version   uint8
        }
        Code  uint8
        Value uint16
    }

with only a single function call:

    ex := Example{}
    _, err := payload.Unmarshal(pld, &ex)

This assignes `0xBE` to the value `ex.MagicByte`,
`ex.Version` is 11, `ex.Code` is `0x04`, and
`ex.Value` is 10.

## The Unmarshal() interface

If the default encoding is not suitable,
then a type can implement the interface `UnmarshalPayload`
to customize the type's unmarshaling.

    int, error UnmarshalPayload([]byte)

This is similar to golang's `UnmarshalBinary` interface
from the `encoding/binary` package, however this interface
also returns the number of consumed bits.
This allows for a more modular composition of the
unmarshaling, e.g. when used in structs.

## Supported Types

  Type  | #Bits | Notes
-------:|:-----:|----
   bool |   8   | 0x00 is false, anything else is true.
  uint8 |   8   | Big-endian
 uint16 |  16   | Big-endian
 uint32 |  32   | Big-endian
 uint64 |  64   | Big-endian
   int8 |   8   | Big-endian, two's complement
  int16 |  16   | Big-endian, two's complement
  int32 |  32   | Big-endian, two's complement
  int64 |  64   | Big-endian, two's complement
 struct |  var  | Concatenating of the binary encodings of all public struct members in order.

## Status

The package is in alpha status. This means the package's code
is subject to change at any time.
