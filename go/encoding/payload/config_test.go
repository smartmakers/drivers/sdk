package payload

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestByteOrder(t *testing.T) {
	SetLittleEndian()

	pld := []byte{01, 00}
	var val uint16
	_, err := Unmarshal(pld, &val)
	assert.NoError(t, err)
	assert.Equal(t, uint16(1), val)

	SetBigEndian()
}
