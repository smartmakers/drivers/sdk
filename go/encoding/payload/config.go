package payload

import "encoding/binary"

var byteOrder binary.ByteOrder = binary.BigEndian

// SetLittleEndian sets the byte order to little endian.
func SetLittleEndian() {
	byteOrder = binary.LittleEndian
}

// SetBigEndian sets the byte order to little endian.
func SetBigEndian() {
	byteOrder = binary.BigEndian
}

// SetCustomByteOrder sets the byte order to a cusom byte order.
//
// This can be used to support more exotic byte orders, e.g.
// middle endian byte order.
func SetCustomByteOrder(bo binary.ByteOrder) {
	byteOrder = bo
}
