package payload

import (
	"encoding/binary"
	"reflect"
	"testing"

	"github.com/stretchr/testify/assert"
)

type testCase struct {
	d string
	v interface{}
	b []byte
	c int
	e string
}

// Bool is a factory for pointers to booleans
func Bool(v bool) *bool {
	return &v
}

// Int is a factory for pointers to integers
func Int(v int) *int {
	return &v
}

func Int16(v int16) *int16 {
	return &v
}

type Impl struct {
	x uint32
}

func (impl *Impl) UnmarshalPayload(bytes []byte) (int, error) {
	impl.x = binary.LittleEndian.Uint32(bytes[0:4])
	return 32, nil
}

var cases = []testCase{
	{
		d: "boolean too short",
		v: Bool(false),
		b: []byte{},
		c: 0,
		e: "payload too short",
	}, {
		d: "boolean false",
		v: Bool(false),
		b: []byte{0x00},
		c: 8,
		e: "",
	}, {
		d: "boolean true",
		v: Bool(true),
		b: []byte{0x01},
		c: 8,
		e: "",
	}, {
		d: "int 0",
		v: Int(0),
		b: []byte{0x00, 0x00, 0x00, 0x00},
		c: 32,
		e: "",
	}, {
		d: "int 4",
		v: Int(4),
		b: []byte{0x00, 0x00, 0x00, 0x04},
		c: 32,
		e: "",
	}, {
		d: "int16 42",
		v: Int16(42),
		b: []byte{0x00, 0x2a},
		c: 16,
		e: "",
	}, {
		d: "struct {true}",
		v: &struct{ B bool }{B: true},
		b: []byte{0x01},
		c: 8,
		e: "",
	}, {
		d: "struct {priv, true}",
		v: &struct {
			priv bool
			Pub  bool
		}{
			priv: false,
			Pub:  true,
		},
		b: []byte{0x01},
		c: 8,
		e: "",
	}, {
		d: "struct {bool, int32}",
		v: &struct {
			Bool bool
			Int  int32
		}{
			Bool: true,
			Int:  4711,
		},
		b: []byte{0x01, 0x00, 0x00, 0x12, 0x67},
		c: 40,
		e: "",
	}, {
		d: "unmarshaler interface",
		v: &Impl{x: 513},
		b: []byte{0x01, 0x02, 0x00, 0x00},
		c: 32,
		e: "",
	}, {
		d: "byte slice",
		v: &[]byte{0x01, 0x02},
		b: []byte{0x01, 0x02},
		c: 16,
		e: "",
	},
}

func TestUnmarshalBytes(t *testing.T) {
	for idx := range cases {
		c := cases[idx]
		t.Run(c.d, func(t *testing.T) {
			t.Helper()
			n := reflect.New(reflect.Indirect(reflect.ValueOf(c.v)).Type()).Interface()
			cnt, err := Unmarshal(c.b, n)

			if c.e == "" {
				assert.NoError(t, err)
				assert.Equal(t, c.c, cnt)
				assert.Equal(t, c.v, n)
			} else {
				assert.Error(t, err, c.e)
				assert.Equal(t, c.c, 0)
			}
		})
	}
}
