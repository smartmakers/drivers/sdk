package payload

// Marshaler marshals payloads to byte arrays.
type Marshaler interface {
	MarshalPayload() ([]byte, int, error)
}
