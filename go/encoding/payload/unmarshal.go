package payload

import (
	"fmt"
	"math"
	"reflect"

	"github.com/pkg/errors"
	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

var (
	errTooShort = errors.New("payload too short")
)

type invalidUnmarshalError struct {
	reflect.Type
}

func (err invalidUnmarshalError) Error() string {
	return fmt.Sprintf("cannot unmarshal to type: %s", err.Name())
}

// The main encoding package tries to make it really easy to decode and encode binary payloads

// Unmarshaler unmarshals objects to binary payloads.
//
// In contrast to encoding.BinaryUnmarshaler, this returns the number of
// bits read as the first return argument.
type Unmarshaler interface {
	UnmarshalPayload([]byte) (int, error)
}

// Unmarshal uses go's reflection and struct tags to unmarshal
// various types into binary payloads.
func Unmarshal(bytes []byte, v interface{}) (int, error) {
	direct := reflect.ValueOf(v)
	if direct.Kind() != reflect.Ptr || direct.IsNil() {
		return 0, invalidUnmarshalError{reflect.TypeOf(v)}
	}

	return unmarshal(bytes, reflect.ValueOf(v))
}

func debug(i int, e error) (int, error) {
	lvl3.Debugf("count: %v\n", i)
	return i, e
}

func unmarshal(bytes []byte, v reflect.Value) (int, error) {
	unmarshaler, rv := indirect(v)
	if unmarshaler != nil {
		return unmarshaler.UnmarshalPayload(bytes)
	}

	switch rv.Kind() {
	case reflect.Bool:
		if len(bytes) < 1 {
			return 0, errTooShort
		}

		rv.SetBool(bytes[0] != 0x00)
		return 8, nil

	case reflect.Int:
		if len(bytes) < 4 {
			return 0, errTooShort
		}

		rv.SetInt(int64(byteOrder.Uint32(bytes[0:4])))
		return 32, nil

	case reflect.Int8:
		if len(bytes) < 1 {
			return 0, errTooShort
		}

		rv.SetInt(int64(int8(bytes[0])))
		return 8, nil

	case reflect.Int16:
		if len(bytes) < 2 {
			return 0, errTooShort
		}

		rv.SetInt(int64(int16(byteOrder.Uint16(bytes[0:2]))))
		return 16, nil

	case reflect.Int32:
		if len(bytes) < 4 {
			return 0, errTooShort
		}

		rv.SetInt(int64(int32(byteOrder.Uint32(bytes[0:4]))))
		return 32, nil

	case reflect.Int64:
		if len(bytes) < 8 {
			return 0, errTooShort
		}

		rv.SetInt(int64(byteOrder.Uint64(bytes[0:8])))
		return 64, nil

	case reflect.Uint:
		if len(bytes) < 4 {
			return 0, errTooShort
		}

		rv.SetUint(uint64(byteOrder.Uint64(bytes[0:4])))
		return 32, nil

	case reflect.Uint8:
		if len(bytes) < 1 {
			return 0, errTooShort
		}

		rv.SetUint(uint64(bytes[0]))
		return 8, nil

	case reflect.Uint16:
		if len(bytes) < 2 {
			return 0, errTooShort
		}

		rv.SetUint(uint64(byteOrder.Uint16(bytes[0:2])))
		return 16, nil

	case reflect.Uint32:
		if len(bytes) < 4 {
			return 0, errTooShort
		}

		rv.SetUint(uint64(byteOrder.Uint32(bytes[0:4])))
		return 32, nil

	case reflect.Uint64:
		if len(bytes) < 8 {
			return 0, errTooShort
		}

		rv.SetUint(uint64(byteOrder.Uint64(bytes[0:8])))
		return 64, nil

	case reflect.Uintptr:
		return 0, errors.New("uintptr not supported")

	case reflect.Float32:
		if len(bytes) < 4 {
			return 0, errTooShort
		}

		f := math.Float32frombits(byteOrder.Uint32(bytes[0:4]))
		rv.SetFloat(float64(f))
		return 32, nil

	case reflect.Float64:
		if len(bytes) < 8 {
			return 0, errTooShort
		}

		f := math.Float64frombits(byteOrder.Uint64(bytes[0:8]))
		rv.SetFloat(f)
		return 64, nil

	case reflect.Complex64:
		return 0, errors.New("complex64 not supported")

	case reflect.Complex128:
		return 0, errors.New("complex128 not supported")

	case reflect.Array:
		return unmarshalArray(bytes, v)

	case reflect.Chan:
		return 0, errors.New("chan unsupported")

	case reflect.Func:
		return 0, errors.New("func unsupported")

	case reflect.Interface:
		return 0, errors.New("interface unsupported")

	case reflect.Map:
		return 0, errors.New("map unsupported")

	case reflect.Ptr:
		return 0, errors.New("ptr unsupported")

	case reflect.Slice:
		eType := rv.Type().Elem()
		sl := reflect.MakeSlice(rv.Type(), 0, 0)
		length := 0
		idx := 0
		for idx < len(bytes) {
			eVal := reflect.New(eType)
			eSize, err := unmarshal(bytes[idx:], eVal)
			if err != nil {
				return 0, err
			}

			if eSize%8 != 0 {
				return 0, errors.New("slice element length must be a multiple of 8")
			}

			// eVal is a uint8 pointer for some reason...
			sl = reflect.Append(sl, eVal.Elem())
			idx += eSize / 8
			length += eSize
		}

		// fill slice with the data
		rv.Set(sl)
		return length, nil

	case reflect.String:
		return 0, errors.New("string unsupported")

	case reflect.Struct:
		return unmarshalStruct(bytes, rv)

	case reflect.UnsafePointer:
		return 0, errors.New("unsafe pointer unsupported")

	default:
		return 0, errors.Errorf(fmt.Sprintf("unsupported: %v", reflect.TypeOf(v)))
	}
}

func unmarshalStruct(bytes []byte, rv reflect.Value) (int, error) {
	// walk over all elements of the struct an handle them in turn
	consumedByStruct := 0
	for idx := 0; idx != rv.NumField(); idx++ {
		field := rv.FieldByIndex([]int{idx})
		if !field.CanInterface() {
			// skipping private fields
			continue
		}

		consumedByField, err := unmarshal(bytes[consumedByStruct/8:], field)
		if err != nil {
			return 0, err
		}

		consumedByStruct += consumedByField
	}

	return consumedByStruct, nil
}

func unmarshalArray(bytes []byte, v reflect.Value) (int, error) {
	elTy := v.Type().Elem()
	length := v.Len()
	bits := elTy.Bits()

	consumedByArray := 0
	for i := 0; i != length; i++ {
		el := v.Index(i)
		consumedByElement, err := unmarshal(bytes[consumedByArray/8:], el)
		if err != nil {
			return 0, err
		}

		consumedByArray += consumedByElement
	}

	return length * bits, nil
}

// indirect walks down v allocating pointers as needed,
// until it gets to a non-pointer.
// If it encounters an Unmarshaler, indirect stops and returns that.
func indirect(v reflect.Value) (Unmarshaler, reflect.Value) {
	// Issue #24153 indicates that it is generally not a guaranteed property
	// that you may round-trip a reflect.Value by calling Value.Addr().Elem()
	// and expect the value to still be settable for values derived from
	// unexported embedded struct fields.
	//
	// The logic below effectively does this when it first addresses the value
	// (to satisfy possible pointer methods) and continues to dereference
	// subsequent pointers as necessary.
	//
	// After the first round-trip, we set v back to the original value to
	// preserve the original RW flags contained in reflect.Value.
	v0 := v
	haveAddr := false

	// If v is a named type and is addressable,
	// start with its address, so that if the type has pointer methods,
	// we find them.
	if v.Kind() != reflect.Ptr && v.Type().Name() != "" && v.CanAddr() {
		haveAddr = true
		v = v.Addr()
	}
	for {
		// Load value from interface, but only if the result will be
		// usefully addressable.
		if v.Kind() == reflect.Interface && !v.IsNil() {
			e := v.Elem()
			if e.Kind() == reflect.Ptr && !e.IsNil() && e.Elem().Kind() == reflect.Ptr {
				haveAddr = false
				v = e
				continue
			}
		}

		if v.Kind() != reflect.Ptr || v.CanSet() {
			break
		}

		// Prevent infinite loop if v is an interface pointing to its own address:
		//     var v interface{}
		//     v = &v
		if v.Elem().Kind() == reflect.Interface && v.Elem().Elem() == v {
			v = v.Elem()
			break
		}

		if v.IsNil() {
			v.Set(reflect.New(v.Type().Elem()))
		}

		if v.Type().NumMethod() > 0 && v.CanInterface() {
			if u, ok := v.Interface().(Unmarshaler); ok {
				return u, reflect.Value{}
			}
		}

		if haveAddr {
			// restore original value after round-trip Value.Addr().Elem()
			v = v0
			haveAddr = false
		} else {
			v = v.Elem()
		}
	}

	return nil, v
}
