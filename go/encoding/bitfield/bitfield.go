package bitfield

import (
	"encoding/binary"
)

type Bitfield []byte

func (bf Bitfield) Bool(at uint) bool {
	pos := at / 8
	offset := at % 8
	return bf[pos]&(0x01<<offset) != 0x00
}

// Byte extracts a single byte starting at a specific bit in the byte array.
func (bf Bitfield) byte(from uint) byte {
	lowByte := from / 8
	offset := from % 8
	if offset == 0 {
		return bf[lowByte]
	}

	lowMask := byte(0xFF) << offset
	lowMasked := bf[lowByte] & lowMask
	lowPart := lowMasked >> offset

	if lowByte == uint(len(bf)-1) {
		return lowPart
	}

	highByte := lowByte + 1
	highMask := byte(uint8(0xFF) >> (8 - offset))
	highMasked := bf[highByte] & highMask
	highPart := highMasked << (8 - offset)
	return lowPart + highPart
}

// Extracts a byte array starting at the given
// start position position and ending one before
// the given end position.
// If to-from is less than 64, the remaining bits
// are filled up with 0 bits.
func (bf Bitfield) bytes(from, to uint) []byte {
	bitLength := to - from
	byteLength := (bitLength + 7) / 8
	res := make([]byte, byteLength)
	offset := from
	for idx := uint(0); idx != byteLength; idx++ {
		by := bf.byte(offset)
		if idx == byteLength-1 {
			remainder := (8*byteLength - bitLength)
			mask := byte(0xFF) >> remainder
			res[idx] = by & mask
		} else {
			res[idx] = by
		}
		offset += 8
	}

	return res
}

func (bf Bitfield) Uint(from, to uint) uint {
	return uint(bf.Uint32(from, to))
}

func (bf Bitfield) Uint8(from, to uint) uint8 {
	// need to extend the byte array first...
	return uint8(bf.bytes(from, to)[0])
}

func (bf Bitfield) Uint16(from, to uint) uint16 {
	// need to extend the byte array first...
	bytes := bf.bytes(from, to)
	if len(bytes) < 2 {
		bytes = append(bytes, make([]byte, 2-len(bytes))...)
	}
	return binary.LittleEndian.Uint16(bytes)
}

func (bf Bitfield) Uint32(from, to uint) uint32 {
	// need to extend the byte array first...
	bytes := bf.bytes(from, to)
	if len(bytes) < 4 {
		bytes = append(bytes, make([]byte, 4-len(bytes))...)
	}

	return binary.LittleEndian.Uint32(bytes)
}

func (bf Bitfield) Uint64(from, to uint) uint64 {
	// need to extend the byte array first...
	bytes := bf.bytes(from, to)
	if len(bytes) < 8 {
		bytes = append(bytes, make([]byte, 8-len(bytes))...)
	}
	return binary.LittleEndian.Uint64(bytes)
}
