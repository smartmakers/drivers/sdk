package bitfield

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestYabby(t *testing.T) {
	payload := []byte{
		0x8b, 0xf3, 0xdc,
		0x7b, 0x94, 0x38,
		0x98, 0x42, 0x78,
		0xb8, 0x5e,
	}

	tests := []struct {
		desc string
		from uint
		to   uint
		exp  uint16
	}{
		{
			desc: "Field 1",
			from: 0,
			to:   4,
			exp:  uint16(11),
		},
		{
			desc: "Field 2",
			from: 4,
			to:   1*8 + 7,
			exp:  uint16(1848),
		},
		{
			desc: "Field 3",
			from: 1*8 + 7,
			to:   3*8 + 4,
			exp:  uint16(6073),
		},
		{
			desc: "Field 4",
			from: 3*8 + 4,
			to:   4*8 + 6,
			exp:  uint16(327),
		},
		{
			desc: "Field 5",
			from: 4*8 + 6,
			to:   5*8 + 6,
			exp:  uint16(226),
		},
		{
			desc: "Field 6",
			from: 5*8 + 6,
			to:   6*8 + 7,
			exp:  uint16(96),
		},
		{
			desc: "Field 7",
			from: 6*8 + 7,
			to:   8*8 + 0,
			exp:  uint16(133),
		},
		{
			desc: "Field 8",
			from: 8*8 + 0,
			to:   9*8 + 0,
			exp:  uint16(120),
		},
		{
			desc: "Field 9",
			from: 9*8 + 0,
			to:   9*8 + 7,
			exp:  uint16(56),
		},
		{
			desc: "Field 10",
			from: 9*8 + 7,
			to:   11 * 8,
			exp:  uint16(189),
		},
	}

	for _, test := range tests {
		t.Run(test.desc, func(t *testing.T) {
			res := Bitfield(payload).Uint16(test.from, test.to)
			assert.Equal(t, test.exp, res)
		})
	}
}

func TestExtractByte(t *testing.T) {
	tests := []struct {
		desc  string
		bytes []byte
		from  uint
		exp   byte
	}{
		{
			desc:  "Distinct bytes 0",
			bytes: []byte{0b00000000, 0b11111111},
			from:  0,
			exp:   0b00000000,
		},
		{
			desc:  "Distinct bytes 1",
			bytes: []byte{0b00000000, 0b11111111},
			from:  1,
			exp:   0b10000000,
		},
		{
			desc:  "Distinct bytes 3",
			bytes: []byte{0b00000000, 0b11111111},
			from:  3,
			exp:   0b11100000,
		},
		{
			desc:  "Distinct bytes 7",
			bytes: []byte{0b00000000, 0b11111111},
			from:  7,
			exp:   0b11111110,
		},
		{
			desc:  "Lower",
			bytes: []byte{0b00110101, 0b11001010},
			from:  0,
			exp:   0b00110101,
		},
		{
			desc:  "Higher",
			bytes: []byte{0b00110101, 0b11001010},
			from:  8,
			exp:   0b11001010,
		},
		{
			desc:  "Middle",
			bytes: []byte{0b00110101, 0b11001010},
			from:  4,
			exp:   0b10100011,
		},
		{
			desc:  "OddOffset",
			bytes: []byte{0b00110101, 0b11001010},
			from:  3,
			exp:   0b01000110,
		},
		{
			desc:  "Longer Slice",
			bytes: []byte{0b111111, 0b11001010, 0b000000},
			from:  11,
			exp:   0b00011001,
		},
	}

	for _, test := range tests {
		t.Run(test.desc, func(t *testing.T) {
			res := Bitfield(test.bytes).byte(test.from)
			assert.Equal(t, test.exp, res)
		})
	}
}

func TestExtractLEUint64(t *testing.T) {
	tests := []struct {
		desc  string
		bytes []byte
		from  uint
		to    uint
		exp   uint64
	}{
		{
			desc:  "Idempotent 0x00",
			bytes: []byte{0b00000000},
			from:  0,
			to:    8,
			exp:   uint64(0),
		},
		{
			desc:  "Idempotent 0xFF",
			bytes: []byte{0b11111111},
			from:  0,
			to:    8,
			exp:   uint64(255),
		},
		{
			desc:  "Split in half",
			bytes: []byte{0b00000000, 0b11111111},
			from:  4,
			to:    12,
			exp:   uint64(240),
		},
		{
			desc:  "Two bytes",
			bytes: []byte{0b00000000, 0b11111111},
			from:  0,
			to:    16,
			exp:   uint64(65280),
		},
	}

	for _, test := range tests {
		t.Run(test.desc, func(t *testing.T) {
			res := Bitfield(test.bytes).Uint64(test.from, test.to)
			assert.Equal(t, test.exp, res)
		})
	}
}
