package lpp

import (
	"encoding/binary"
	"errors"
)

// Accelerometer is a 16-bit integer,
// that encodes acceleration in 0.001G.
type Accelerometer struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`
}

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a Accelerometer.
func (acc *Accelerometer) UnmarshalBinary(data []byte) error {
	if len(data) != 6 {
		return errors.New("accelerometer must have size 6")
	}

	acc.X = float64(int16(binary.BigEndian.Uint16(data[0:2]))) * 0.001
	acc.Y = float64(int16(binary.BigEndian.Uint16(data[2:4]))) * 0.001
	acc.Z = float64(int16(binary.BigEndian.Uint16(data[4:6]))) * 0.001
	return nil
}
