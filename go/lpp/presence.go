package lpp

import (
	"errors"
)

// Presence is a 8-bit number,
// that encodes presence information.
type Presence byte

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for presence.
func (tmp *Presence) UnmarshalBinary(data []byte) error {
	if len(data) != 1 {
		return errors.New("Presence must have size 1")
	}

	*tmp = Presence(data[0])
	return nil
}
