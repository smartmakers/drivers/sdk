package lpp

import (
	"encoding/binary"
	"errors"
)

// Illuminance is a 16-bit integer,
// that encodes illuminance in 1 Lux steps.
type Illuminance int16

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a Illuminance.
func (tmp *Illuminance) UnmarshalBinary(data []byte) error {
	if len(data) != 2 {
		return errors.New("Illuminance must have size 2")
	}

	*tmp = Illuminance(binary.BigEndian.Uint16(data[0:2]))
	return nil
}
