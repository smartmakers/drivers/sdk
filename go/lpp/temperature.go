package lpp

import (
	"encoding/binary"
	"errors"
)

// Temperature is a 16-bit integer,
// that encodes temperature in 1/10 °C steps.
type Temperature float32

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a Temperature.
func (tmp *Temperature) UnmarshalBinary(data []byte) error {
	if len(data) != 2 {
		return errors.New("temperature must have size 2")
	}

	*tmp = Temperature(float32(int16(binary.BigEndian.Uint16(data[0:2]))) * 0.1)
	return nil
}
