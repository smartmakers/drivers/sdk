package lpp

import (
	"errors"
)

// Humidity is a 16-bit integer,
// that encodes humidity in steps of 0.5%.
type Humidity float32

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a relative humidity.
func (tmp *Humidity) UnmarshalBinary(data []byte) error {
	if len(data) != 1 {
		return errors.New("Humidity must have size 1")
	}

	*tmp = Humidity(float32(data[0]) * 0.5)
	return nil
}
