package lpp

import (
	"encoding/binary"
	"errors"
)

// Gyrometer is a 6-byte orientation
// with X, Y, and Z components.
// Each part is encoded in two bytes.
type Gyrometer struct {
	X float64 `json:"x"`
	Y float64 `json:"y"`
	Z float64 `json:"z"`
}

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a gyrometer.
func (gy *Gyrometer) UnmarshalBinary(data []byte) error {
	if len(data) != 6 {
		return errors.New("gyrometer must have size 6")
	}

	gy.X = float64(int16(binary.BigEndian.Uint16(data[0:2]))) * 0.01
	gy.Y = float64(int16(binary.BigEndian.Uint16(data[2:4]))) * 0.01
	gy.Z = float64(int16(binary.BigEndian.Uint16(data[4:6]))) * 0.01
	return nil
}
