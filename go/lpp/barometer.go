package lpp

import (
	"encoding/binary"
	"errors"
)

// Barometer is a 16-bit integer,
// that encodes barometry information in 0.1 hPa steps.
type Barometer float32

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a Barometer.
func (tmp *Barometer) UnmarshalBinary(data []byte) error {
	if len(data) != 2 {
		return errors.New("barometer must have size 2")
	}

	*tmp = Barometer(float32(int16(binary.BigEndian.Uint16(data[0:2]))) * 0.1)
	return nil
}
