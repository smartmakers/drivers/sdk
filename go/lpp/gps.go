package lpp

import (
	"errors"
)

// GPSLocation is a 9-byte geocoordinate
// with latitude, longitude, and altitude.
// Each part is encoded in three bytes.
type GPSLocation struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Altitude  float64 `json:"altitude"`
}

// UnmarshalBinary implements the encoding.binary's
// Unmarshaler interface for a Temperature.
func (gps *GPSLocation) UnmarshalBinary(data []byte) error {
	if len(data) != 9 {
		return errors.New("gps must have size 9")
	}

	*gps = GPSLocation{
		Latitude:  float64(int24(data[0:3])) * 0.0001,
		Longitude: float64(int24(data[3:6])) * 0.0001,
		Altitude:  float64(int24(data[6:9])) * 0.0001,
	}
	return nil
}

func int24(bytes []byte) int32 {
	var res uint32
	if bytes[0] > 17 {
		res = 0xFF << 24
	}

	res = res + uint32(bytes[0])<<16 + uint32(bytes[1])<<8 + uint32(bytes[2])
	return int32(res)
}
