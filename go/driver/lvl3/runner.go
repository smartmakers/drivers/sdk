package lvl3

import (
	"encoding/json"
	"fmt"
	"os"
)

// TODO:
// * check if we can turn UnmarshalPropertySet into NewPropertySet
// * check if we want to parametrize this by event type
// * consistent naming, including Event, Request, Response
// * consider making the input streamable (one JSON on one line per req/resp)
// * with streamable input, we can add log lines, too
//   { "type": "log", "level": "info", "message": "...", "fields": {"foo": "bar"} }
//   { "response": { "downlinks": [ "payload": "01", "port": 10], "state": {...}}}
const (
	initialize   = "initialize"
	finalize     = "finalize"
	uplink       = "uplink"
	confirmation = "confirmation"
	config       = "config"
	action       = "action"
)

// Run is the primary entry point into this package
//
// The first argument is the driver to execute
// and the second argument is usually os.Args().
func Run(driver Driver, args []string) {
	resp, codedErr := run(driver, args[1:])
	if codedErr != nil {
		// handle error
		bytes, err := json.Marshal(struct {
			Error codedError `json:"error"`
		}{
			Error: codedError{
				Code:    codedErr.Code,
				Message: codedErr.Error(),
			},
		})

		if err != nil {
			fmt.Print(`{"error": { "code": "internal", "message": "failed marshaling error"}}`)
			os.Exit(1)
		}

		fmt.Print(string(bytes))
		os.Exit(1)
	}

	bytes, err := json.Marshal(struct {
		Response interface{} `json:"response"`
	}{
		Response: resp,
	})
	if err != nil {
		// handle error
		bytes, err := json.Marshal(struct {
			Error codedError `json:"error"`
		}{
			Error: codedError{
				Code:    "internal",
				Message: err.Error(),
			},
		})

		if err != nil {
			fmt.Print(`{"error": { "code": "internal", "message": "failed marshaling error"}}`)
			os.Exit(1)
		}

		fmt.Print(string(bytes))
		os.Exit(1)
	}

	fmt.Print(string(bytes))
}

type codedError struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

// run creates and executes the runner and converts panics to coded (internal) errors.
func run(driver Driver, args []string) (resp interface{}, err *CodedError) {
	var coded CodedError
	defer func() {
		p := recover()
		if p != nil {
			if e, ok := p.(error); ok {
				err = asCodedError(e)
			} else {
				err = InternalErrorf("panic: %s", p)
			}
		}
	}()

	if len(args) < 1 {
		coded = SyntaxError(`no event type given`)
		return nil, &coded
	}

	// Run the actual driver
	r := runner{driver}
	return cleanupResponse(r.handle(args[0]))
}

// cleanupResponse cleans up the driver's response.
//
// This also converts any error passed in to a coded error.
func cleanupResponse(resp *Response, err error) (*Response, *CodedError) {
	if resp != nil && resp.Downlinks == nil {
		resp.Downlinks = []Downlink{}
	}

	return resp, asCodedError(err)
}

// The runner is a thin convenience wrapper around the Driver.
//
// A runner unmarshals requests from the command line,
// executes the drivers decode or encode function,
// marshals the response and prints it out on stdout.
type runner struct {
	Driver
}

func (r *runner) handle(event string) (*Response, error) {
	switch event {
	case initialize:
		return r.initialize()
	case finalize:
		return r.finalize()
	case uplink:
		return r.uplink()
	case confirmation:
		return r.confirmation()
	case config:
		return r.config()
	case action:
		return r.action()
	default:
		return nil, UnsupportedError(event)
	}
}

func (r *runner) uplink() (*Response, error) {
	evt := UplinkEvent{}
	if err := decode(&evt); err != nil {
		return nil, err
	}

	normalize(&evt.Config)
	normalize(&evt.State)

	return r.Driver.Uplink(evt)
}

func (r *runner) config() (*Response, error) {
	evt := ConfigEvent{}
	if err := decode(&evt); err != nil {
		return nil, err
	}

	normalize(&evt.Config)
	normalize(&evt.State)

	return r.Driver.Config(evt)
}

func (r *runner) initialize() (*Response, error) {
	evt := InitializeEvent{}
	if err := decode(&evt); err != nil {
		return nil, err
	}

	normalize(&evt.Config)
	normalize(&evt.State)

	return r.Driver.Initialize(evt)
}

func (r *runner) finalize() (*Response, error) {
	evt := FinalizeEvent{}
	if err := decode(&evt); err != nil {
		return nil, err
	}

	normalize(&evt.Config)
	normalize(&evt.State)

	return r.Driver.Finalize(evt)
}

func (r *runner) confirmation() (*Response, error) {
	evt := ConfirmationEvent{}
	if err := decode(&evt); err != nil {
		return nil, err
	}

	normalize(&evt.Config)
	normalize(&evt.State)

	return r.Driver.Confirmation(evt)
}

func (r *runner) action() (*Response, error) {
	evt := ActionEvent{}
	if err := decode(&evt); err != nil {
		return nil, err
	}

	normalize(&evt.Config)
	normalize(&evt.State)

	return r.Driver.Action(evt)
}

// Generic decoding function
func decode(v interface{}) error {
	dec := json.NewDecoder(os.Stdin)
	// dec.DisallowUnknownFields()
	err := dec.Decode(&v)
	if err != nil {
		return SyntaxError(err.Error())
	}

	return nil
}

// normalize makes sure there's always valid json in the Event's State.
func normalize(msg *json.RawMessage) {
	if len(*msg) == 0 {
		msg.UnmarshalJSON([]byte{'{', '}'})
	}
}
