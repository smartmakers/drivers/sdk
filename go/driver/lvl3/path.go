package lvl3

import "strings"

type Path string

func (p Path) Split() []string {
	return strings.Split(string(p), ".")
}
