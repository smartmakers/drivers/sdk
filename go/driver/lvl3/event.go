package lvl3

import (
	"encoding/json"
	"time"

	"gitlab.com/smartmakers/drivers/sdk/go/encoding/hex"
)

// Event is the base type for all event types.
type Event struct {
	// The time at which this event happened in the thingsHub
	Time   time.Time       `json:"time"`
	State  json.RawMessage `json:"state"`
	Config json.RawMessage `json:"config"`
}

// A UplinkEvent occurs when a device sends an
// uplink over a network.
//
// UplinkEvent is an upstream event, i.e. data
// flows from the device to the application/user.
type UplinkEvent struct {
	Event   `json:",inline"`
	Payload hex.Hex `json:"payload"`
	Port    int     `json:"port"`

	Metadata *UplinkMetadata `json:"metadata"`
	// TODO: add more uplink info when we work on the metadata ticket
	// * Retransmission		LT
	// * FrameCounterUp		LTK
	// * FrameConterDown	  K
	// * IsConfirmedUplink	 TK
	// * NetworkTimestamp	LTK
	// * DevEUI				LT
	// * Modulation			 TK
	// * Frequency			LTK
	// * Datarate			LTK
	// * Bitrate			 T
	// * Receptions:
	// ** RSSI				LTK
	// ** SNR				LTK
	// ** Timestamp			LTK
	// ** Channel			 TK
	// ** GatewayID			LTK
	// ** Latitude			LTK
	// ** Longitude			LTK
	// ** Altitude			  K
}

// A ConfigEvent occurs when the user requests
// a change to the device's configuration.
//
// ConfigEvent is an downstream event, i.e. data
// flows from the user to the device.
type ConfigEvent struct {
	Event `json:",inline"`

	// A ConfigRequestedEvent currently does not
	// have additional fields, because the
	// configuration is passed in with all event types
	// anyways.
}

// A ConfirmationEvent occurs when the device
// confirms the reception of a previously sent downlink.
//
// ConfirmDownlink is an upstream request, i.e. data
// flows from the device to the application/user.
type ConfirmationEvent struct {
	Event `json:",inline"`
}

// An ActionEvent occurs when the users
// asks the device to execute an action.
//
// An ActionRequestedEvent is a an downstream event,
// i.e. going primarily from the user to the device.
//
// The event contains an ordered list of actions
// to execute.
// An action is something that the device does that
// does not change the state of the device, e.g.
// blink an LED or send its own configuration to
// the server.
type ActionEvent struct {
	Event `json:",inline"`

	// Actions ist a list of properties that the user might desire to execute.
	//
	// Action must be executed in order by the driver.
	// Each action is a map with one element: a mapping from the action's
	// path to it's set of (named) parameters.
	//
	// Action's are currently always null-ary, i.ie. the set of parameters is always empty.
	Actions []map[Path]json.RawMessage `json:"actions,omitempty"`
}

// An InitializeEvent is called when the driver is assigned to a device.
// This is a lifecycle event and the first event called on the driver for
// a device.
type InitializeEvent struct {
	Event `json:",inline"`

	// TODO: this might be a good time to check if the driver can be
	// work in the given environment, e.g. a driver that requires downlinks#
	// could check here if downlinks are enabled. However, at the time of
	// assigning the driver, the network might not yet have been assigned,
	// so there's some additional design required here first.
}

// A FinalizeEvent is called when a driver is unassigned from a device.
// This is a lifecycle event and called before the driver is unasssigned from
// the device.
// Note that the device will be deleted immediately after unassigning the driver,
// i.e. any request to send downlinks will be ignored.
type FinalizeEvent struct {
	Event `json:",inline"`
}
