package lvl3

import "time"

// UplinkMetadata contains additional, unreliable data about
// an event.
// Avoid relying on this information when processing
// events.
type UplinkMetadata struct {
	Device  *DeviceMetadata  `json:"device,omitempty"`
	Network *NetworkMetadata `json:"network,omitempty"`
}

type DeviceMetadata struct {
	ID     string `json:"id,omitempty"`
	Name   string `json:"name,omitempty"`
	Driver string `json:"driver,omitempty"`
}

type NetworkMetadata struct {
	RSSI        int               `json:"rssi,omitempty"`
	SNR         *float64          `json:"snr,omitempty"`
	Receptions  []UplinkReception `json:"receptions,omitempty"`
	GeoPosition *GeoPosition      `json:"geo_position,omitempty"`
}

type UplinkReception struct {
	Gateway     string       `json:"gateway,omitempty"`
	RSSI        int          `json:"rssi,omitempty"`
	Time        time.Time    `json:"time,omitempty"`
	SNR         *float64     `json:"snr,omitempty"`
	GeoPosition *GeoPosition `json:"geo_position,omitempty"`
}

type GeoPosition struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Margin    int     `json:"margin"`
}
