package lvl3

import (
	"fmt"
	"os"
)

func Info(args ...interface{}) {
	os.Stderr.Write([]byte(fmt.Sprint(args...)))
}

func Infof(msg string, args ...interface{}) {
	os.Stderr.Write([]byte(fmt.Sprintf(msg, args...)))
}

func Debug(args ...interface{}) {
	os.Stderr.Write([]byte(fmt.Sprint(args...)))
}

func Debugf(msg string, args ...interface{}) {
	os.Stderr.Write([]byte(fmt.Sprintf(msg, args...)))
}