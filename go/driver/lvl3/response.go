package lvl3

import (
	"time"

	"gitlab.com/smartmakers/drivers/sdk/go/encoding/hex"
)

// A Response is used by a driver to instruct the
// platform on which downlinks to send,
// what to persist until the next request happens,
// and what to send to the application/user.
type Response struct {
	// Downlinks is the list of downlink messages that
	// the driver wants the framework to send to the
	// device.
	//
	// The framework will attempt to send downlinks the
	// order of this array with Downlinks[0] being sent
	// first.
	Downlinks Downlinks `json:"downlinks,omitempty"`

	// The State contains all information that the driver
	// needs to store so that it can access this information
	// during handling of the next event.
	//
	// If the state is nil, it will remain unchanged.
	State interface{} `json:"state,omitempty"`

	// Updates contains time-stamped updates of values
	// measured by the device sent out to the server.
	Updates []Update `json:"updates,omitempty"`

	// DeviceHealth contains time-stamped data decoded
	// from device's uplink related to the device's health
	// in thingshub standard format.
	// Please follow the recommended Response format
	// so that the ThingsHub system can identify these details.
	DeviceHealth []Update `json:"device_health,omitempty"`
}

func (resp *Response) QueueDownlink(payload hex.Hex, port uint8) {
	// note: appending to a nil slice is safe
	resp.Downlinks = append(resp.Downlinks, Downlink{
		Payload: payload,
		Port:    port,
	})
}

func (resp *Response) QueueConfirmedDownlink(payload hex.Hex, port uint8) {
	resp.Downlinks = append(resp.Downlinks, Downlink{
		Payload:   payload,
		Port:      port,
		Confirmed: true,
	})
}

func (resp *Response) AddUpdate(t time.Time, props interface{}) {
	resp.Updates = append(resp.Updates, Update{Time: t, Properties: props})
}

func (resp *Response) AddDeviceHealth(t time.Time, props interface{}) {
	resp.DeviceHealth = append(resp.DeviceHealth, Update{Time: t, Properties: props})
}

// Downlinks is queue of dowlinks to be sent in order.
type Downlinks []Downlink

// Queue adds an unconfirmed downlink to the queue.
func (d *Downlinks) Queue(payload hex.Hex, port uint8) {
	*d = append(*d, Downlink{
		Payload:   payload,
		Port:      port,
		Confirmed: false,
	})
}

// QueueConfirmed adds a confirmed downlink to the queue.
func (d *Downlinks) QueueConfirmed(payload hex.Hex, port uint8) {
	*d = append(*d, Downlink{
		Payload:   payload,
		Port:      port,
		Confirmed: true,
	})
}

// A Downlink is a message from the driver to the device.
type Downlink struct {
	Payload   hex.Hex `json:"payload"`
	Port      uint8   `json:"port"`
	Confirmed bool    `json:"confirmed,omitempty"`
}

// An Update usually is the change of a number of
// values measured by the device at a specific point of time.
//
// Note that this time does not need to be now,
// but can be at any point of time in the past (and possibly the future).
type Update struct {
	Time       time.Time   `json:"time"`
	Properties interface{} `json:"properties"`
}
