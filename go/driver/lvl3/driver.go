// Package lvl3 implementes the third iteration of thingsHub driver framework.
//
// Drivers based on this package are called Level 3 Drivers
// and can be written more flexibly and can thereby
// cover more different use cases.
//
// Comparison to level 2 drivers:
// * Renamed State to PropertySet where this makes sense
// * Only use State for the actual state
// * Renamed desired state to config
// * Allow the same reactions to an event for all event types,
//   i.e. always allow sending downlinks, sending updates,
//   and storing in the state.
// * Added actions as a separate concept
package lvl3

// Driver is the core interface for implementing drivers.
//
// Create a type which implements this interface, then,
// in your main function, call it with Run(driver, os.Args)
// and this package will take care of the rest.
type Driver interface {
	// Uplink processes an UplinkEvent.
	//
	// This event happens when a device sent an uplink through a network.
	Uplink(UplinkEvent) (*Response, error)

	// Config procesces a ConfigEvent.
	//
	// This event happens when the user requested a configuration change.
	Config(ConfigEvent) (*Response, error)

	// Future Work:

	// ConfirmDownlink is called when the delivery of a previously sent downlink
	// was confirmed by the network.
	// For confirmed downlinks, this means the device confirms reception of the downlink
	// (with the next uplink), for unconfirmed downlinks this happens when the
	// gateway confirms the succesfull sending of the downlink.
	Confirmation(ConfirmationEvent) (*Response, error)

	// Execute Action is called when the user requested execution of an action
	// by the device.
	// Actions are non-state changing, e.g. a reboot command or request to
	// transmit a specific value once (mostly for class C devices).
	Action(ActionEvent) (*Response, error)

	// Initialize is called then the driver is assigned to the device.
	Initialize(InitializeEvent) (*Response, error)

	// Finalize is called right before a device is deleted or when
	// another driver is assigned to this device.
	Finalize(FinalizeEvent) (*Response, error)
}

// DefaultDriver is an implementation of the Driver interface
// that returns UnsupporterError for all event types.
//
// This is a just a bit of convenience for writing drivers
// that do not require an implementation for all event handlers.
type DefaultDriver struct {
}

// Initialize returns an UnsupportedError
func (drv *DefaultDriver) Initialize(InitializeEvent) (*Response, error) {
	resp := Response{}
	return &resp, UnsupportedError(initialize)
}

// Finalize returns an UnsupportedError
func (drv *DefaultDriver) Finalize(FinalizeEvent) (*Response, error) {
	resp := Response{}
	return &resp, UnsupportedError(finalize)
}

// Uplink returns an UnsupportedError
func (drv *DefaultDriver) Uplink(UplinkEvent) (*Response, error) {
	resp := Response{}
	return &resp, UnsupportedError(uplink)
}

// Confirmation returns an UnsupportedError
func (drv *DefaultDriver) Confirmation(ConfirmationEvent) (*Response, error) {
	resp := Response{}
	return &resp, UnsupportedError(confirmation)
}

// Config returns an UnsupportedError
func (drv *DefaultDriver) Config(ConfigEvent) (*Response, error) {
	resp := Response{}
	return &resp, UnsupportedError(config)
}

// Action returns an UnsupportedError
func (drv *DefaultDriver) Action(ActionEvent) (*Response, error) {
	resp := Response{}
	return &resp, UnsupportedError(action)
}
