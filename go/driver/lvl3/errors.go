package lvl3

import (
	"fmt"

	"github.com/pkg/errors"
)

const (
	syntax      = "syntax"
	internal    = "internal"
	unsupported = "unsupported"
	user        = "user"
)

// CodedError is an error with a well-defined error code.
type CodedError struct {
	// Code is an index into the error above
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (err CodedError) Error() string {
	return fmt.Sprintf("%s: %s", err.Code, err.Message)
}

// asCodedError converts any error type to one of the known, coded errors.
// Anything that's not known already is treated as an InternalError.
func asCodedError(err error) *CodedError {
	if err == nil {
		return nil
	}

	// If the root cause of this error is a coded error,
	// then we wrap the message of the whole error chain
	// into another coded error with the same code.
	// This way, we can reuse the coded errors unmarshaling
	// to json to conveniently write it out in the required
	// format.
	cause := errors.Cause(err)
	if coded, ok := cause.(CodedError); ok {
		return &CodedError{
			Code:    coded.Code,
			Message: err.Error(),
		}
	}

	return &CodedError{
		Code:    internal,
		Message: err.Error(),
	}
}

// SyntaxError indicates that the driver was used incorrectly by the framework.
//
// A SyntaxError shall be used to indicate that the
// framework that runs the driver provided incorrect syntax,
// e.g. when no subcommand was provided (syntax on the command line)
// or when the syntax of the json passed in via stdin was invalid.
// This error usually indicates not that the end user provided something
// wrong, but that the system that executed the driver did.
func SyntaxError(reason string) CodedError {
	return CodedError{syntax, reason}
}

// SyntaxErrorf returns a SyntaxError with a format string.
func SyntaxErrorf(reason string, args ...interface{}) CodedError {
	return CodedError{syntax, fmt.Sprintf(reason, args...)}
}

// IsUsageError returns true if and only if the root cause of the
// error is a usage error.
func IsUsageError(err error) bool {
	return isCoded(err, syntax)
}

// InternalError creates a new internal error with the given reason.
//
// An internal error should be returned when something unexpected
// happened in the driver and the driver cannot recover from this.
func InternalError(message string) CodedError {
	return CodedError{internal, message}
}

// InternalErrorf creates a new internal error with the given reason
func InternalErrorf(message string, args ...interface{}) *CodedError {
	return &CodedError{internal, fmt.Sprintf(message, args...)}
}

// IsInternalError returns true if the error is internal
func IsInternalError(err error) bool {
	return isCoded(err, internal)
}

// UnsupportedError creates a new internal error with the given reason.
//
// An unsupported error should be returned when the requested operation
// is not supported by the driver, e.g. when the driver does not
// support sending downlinks.
func UnsupportedError(message string) CodedError {
	return CodedError{unsupported, message}
}

// UnsupportedErrorf creates a new internal error with the given reason
func UnsupportedErrorf(message string, args ...interface{}) CodedError {
	return CodedError{unsupported, fmt.Sprintf(message, args...)}
}

// IsUnsupportedError returns true if the error is internal
func IsUnsupportedError(err error) bool {
	return isCoded(err, unsupported)
}

// UserError creates a new internal error with the given reason.
//
// A user error should be returned when the user's request is
// invalid.
//
// Note that this error is only expected to happen for downstream
// events, i.e. those triggered by a user action, not for upstream
// events, i.e. those triggered by the device itself.
func UserError(message string) CodedError {
	return CodedError{user, message}
}

// UserErrorf creates a new internal error with the given reason
func UserErrorf(message string, args ...interface{}) CodedError {
	return CodedError{user, fmt.Sprintf(message, args...)}
}

// IsUserError returns true if the error is internal
func IsUserError(err error) bool {
	return isCoded(err, user)
}

func isCoded(err error, code string) bool {
	if coded, ok := errors.Cause(err).(CodedError); ok {
		return coded.Code == code
	}

	return false
}
