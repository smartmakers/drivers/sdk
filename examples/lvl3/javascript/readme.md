# Javascript LVL3 Example Driver

This driver is an example project for running decoder javascrript code, as provided by some manufacturers,
as lvl3 drivers.

This is intendend for use with legacy javascript code, but it is not recommended for new projects.

## Quick Summary

The example uses a golang-based javascript interpreted and embeds the javascript file in the driver itself
using a virtual file system. This creates a strand-alone driver that can be used to quickly get a
javascript decoder running a s adriver.

### How to Use

1. Copy the files `.project`, `main.go`, `generator/generate.go` to a new directory.
2. Add a file `schema.yaml` with the following content

       version "3.0"
       properties: {}

3. Change `.project` to match the driver / device.
4. Create the directory `assets/` an copy the javascript to `assets/decoder.js`.
5. Change the script to automatically execute the decoder function with the parameters `payload` and `port`.
   Make sure this is called as the last statement of the script, e.g. when the script's main decode
   function is called `decode` with the first argument being the payload as a byte array and the second
   argument the port number, run `decode(payload, port)` in the last line of the script.
6. Run `go generate` to generate the file `decoder.go` from `assets/decoder.js`.
7. Run `drivers build` to compile the driver.
8. Create the file `test.yaml` and add an example uplink payload:

       name: "Tests"
       tests:
       - name: "First uplink payload"
         uplink:
           payload: <copy payload here>
           port: <copy port here>
        reponse: {}

9. Run `drivers test` to execute this test file.
10. Fix any errors reported by the test tool or add missing properties to the schema until the tests pass.
11. Add more tests and repeat step 9.
12. Login to your thingsHub with `drivers login <host> <user>` and your password, then uplink with `drivers push -t <versio>n`.
   During development, it is recommended to use versions `0.x.y`. Once the driver's schema is stable,
   use positive version numbers `1.x.y`.
