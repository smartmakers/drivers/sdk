package main

import (
	"encoding/json"

	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

// The Config is the device's user-settable configuration.
type Config struct {
	// Send to the device to make the device send confirmed data up
	Confirmed *bool `json:"confirmed,omitempty"`

	// Not sent to the device: used in the driver to multiply
	// the temperature with a given number.
	Multiplier int `json:"multiplier,omitempty"`
}

func configUplink(evt lvl3.UplinkEvent) (*lvl3.Response, error) {
	resp := lvl3.Response{}

	var state State
	err := json.Unmarshal(evt.State, &state)
	if err != nil {
		return nil, err
	}

	if len(evt.Payload) != 1 {
		return nil, lvl3.UserError("config payload must have size 1")
	}

	// do something with the state
	confirmed := evt.Payload[0] != 0x00

	state.Config.Confirmed = &confirmed
	resp.State = state
	return &resp, nil
}

func (drv *driver) Config(evt lvl3.ConfigEvent) (*lvl3.Response, error) {
	resp := new(lvl3.Response)

	var state State
	err := json.Unmarshal(evt.State, &state)
	if err != nil {
		return nil, err
	}

	var config Config
	err = json.Unmarshal(evt.Config, &config)
	if err != nil {
		return nil, err
	}

	if config.Confirmed != nil {
		// some setting for confirmed is requested
		if state.Confirmed == nil || *state.Confirmed != *config.Confirmed {
			// and we don't know the option in the state or we don't
			// have it set correctly, so we qeueue a downlink for it
			if *config.Confirmed == true {
				resp.Downlinks.Queue([]byte{byte(0x01)}, 2)
			} else {
				resp.Downlinks.Queue([]byte{byte(0x00)}, 2)
			}
		}
	}

	resp.State = state

	return resp, nil
}
