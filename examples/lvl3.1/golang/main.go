package main

import (
	"encoding/binary"
	"encoding/json"
	"os"

	"github.com/pkg/errors"
	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl3"
)

const (
	// ConfigPort is the port on which configuration messages are sent
	ConfigPort = 1

	// DataPort is the port on which data messages are sent
	DataPort = 2
)

func main() {
	lvl3.Run(&driver{}, os.Args)
}

type driver struct {
	lvl3.DefaultDriver
}

func (drv *driver) Uplink(evt lvl3.UplinkEvent) (*lvl3.Response, error) {
	switch evt.Port {
	case ConfigPort:
		return configUplink(evt)
	case DataPort:
		return dataUplink(evt)
	default:
		return nil, lvl3.UserError("invalid port number")
	}
}

// The State keeps the device's internal management information.
type State struct {
	// The firmware version, sent once in a configuration message
	FirmwareVersion string `json:"firmware_version,omitempty"`

	// The device's actual config (as reported by the device)
	Config `json:",inline"`
}

func dataUplink(evt lvl3.UplinkEvent) (*lvl3.Response, error) {
	resp := lvl3.Response{}

	var state State
	err := json.Unmarshal(evt.State, &state)
	if err != nil {
		return nil, err
	}

	if len(evt.Payload) != 2 {
		return nil, lvl3.UserError("data payload must have size 2")
	}

	temp := binary.BigEndian.Uint16(evt.Payload)
	resp.Updates = []lvl3.Update{
		{
			Time: evt.Time,
			Properties: Update{
				Temperature: int32(temp),
			},
		},
	}

	return &resp, nil
}

// The Update contains all data send to the consumer.
type Update struct {
	// The measured temperature, as sent by the device (and after being multiplied)
	Temperature int32 `json:"temperature"`
}

func (drv *driver) Action(evt lvl3.ActionEvent) (*lvl3.Response, error) {
	resp := lvl3.Response{}

	for _, acts := range evt.Actions {
		for name := range acts {
			switch name {
			case "reboot":
				resp.QueueDownlink([]byte{0x01, 0x02}, 1)

			case "reset":
				resp.QueueDownlink([]byte{0x01, 0x03}, 1)
				resp.State = map[string]interface{}{}

			default:
				return nil, errors.Errorf("invalid action: %s", name)
			}
		}
	}
	return &resp, nil
}
