onUplink = function(config, state, payload, port, time) {
    return {
        "state": state,
        "downlinks": [
            {
                "payload": payload,
                "port": port,
                "confirmed": false
            }
        ],
        "updates": [
            {
                "time": time-(60*60),
                "properties": {
                    "temperature": 25
                }            
            }
        ]
    }
}

onConfig = function(config, state) {
    return {
        "state": state,
        "downlinks": [
            {
                "payload": [0x01, 0x02],
                "port": 3,
                "confirmed": true
            }
        ]
    }
}

onActions = function(config, state, actions) {
    var dl = []
    for (var i = 0; i != actions.length; i++) {
        var action = actions[i]
        if ("reboot" in action) {
            dl = dl.concat({
                "payload": [0x42],
                "port": 13
            })
        }
    }

    return { "downlinks": dl }
}