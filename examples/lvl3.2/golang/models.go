package main

type Uplink struct {
	Temperature   float64 `json:"temperature"`
	Voltage       float64 `json:"voltage"`
	ExternalPower bool    `json:"external_power"`
}

// DeviceHealth
type DeviceHealth struct {
	Battery Battery `json:"battery"`
}

// Battery provides the state of the battery of the device
// as a part of device health
type Battery struct {
	ChargeVoltage float64 `json:"charge_voltage"`
	PowerSource   string  `json:"power_source"`
}

// The Update contains all data send to the consumer.
type Update struct {
	Temperature   int32   `json:"temperature"`
	Voltage       float64 `json:"voltage"`
	ExternalPower bool    `json:"external_power"`
}
