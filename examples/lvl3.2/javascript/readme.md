# Javascript Level 3.2 Example Driver

This driver is an example project for running decoder javascrript code, as provided by some manufacturers,
as a level 3.2 driver.

## Quick Summary

The example uses a golang-based javascript interpreted (called Otto) and embeds the javascript file
in the driver itself using golang's `embed` package.
This creates a stand-alone driver javascript, which makes full use of the thingsHub
driver framework.

### How to Use

1. Copy the files `.project`, `main.go`, `schema.yaml`, `script.js`, and `test.yaml` to a new directory.
3. Modify the file `.project` to match the device of your choice.
5. Change the script `script.js` to according to the device's (payload) specification.
7. Run `drivers build` to compile the driver.
8. Adapt the file `test.yaml` to contain tests which match the real device's behavior.
9. Run `drivers test` to execute the driver against this test suite.
10. Fix any errors reported by the test tool or add missing properties to the schema until the tests pass.
11. Add more tests and repeat step 9.
12. Login to your thingsHub with `drivers login <host> <user>` and your password, then uplink with `drivers push -t <versio>n`.
   During development, it is recommended to use versions `0.x.y`. Once the driver's schema is stable,
   use positive version numbers `1.x.y`.
