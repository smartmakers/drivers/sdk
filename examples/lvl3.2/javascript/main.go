package main

import (
	_ "embed"

	"gitlab.com/smartmakers/drivers/sdk/go/js"
)

//go:embed script.js
var script []byte

func main() { js.RunDriver(script) }
