package main

import (
	"os"

	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl2"
)

// Driver is the main driver itself.
type Driver struct{}

func main() {
	lvl2.Run(&Driver{}, os.Args[1:])
}
