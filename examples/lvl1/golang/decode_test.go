package main

import (
    "testing"

    . "gitlab.com/smartmakers/drivers/sdk/go/testing"
)

func dec(payload []byte, port int) (interface{}, error) {
    return decode(payload, port)
}

func TestDecodeOk(t *testing.T) {
    TestString(t, "0100CD022A0701F0", 5, dec, `{"temperature":20.5,"humidity":42,"vdd":496}`)
}

func TestDecodeWithCO2DataOk(t *testing.T) {
    TestString(t, "0100CD022A06ffff0701F0", 5, dec, `{"temperature":20.5,"humidity":42,"vdd":496}`)
}

func TestDecodeInvalidPortErr(t *testing.T) {
    TestError(t, "0100CD022A0701F0", 1, dec, "invalid port: expected 5, got 1")
}
