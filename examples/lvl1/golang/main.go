package main

import (
	"os"

	"gitlab.com/smartmakers/drivers/sdk/go/driver/lvl1"
)

func main() {
	drv := lvl1.New()
	drv.Decoder = decode
	drv.Run(os.Args[1:])
}
