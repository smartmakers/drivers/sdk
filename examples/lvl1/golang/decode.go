/**
 * Elsys ERS-lite device:
 * https://www.elsys.se/en/wp-content/uploads/sites/7/2016/09/ERS-lite-folder.pdf
 *
 * Official documentation for decoding of payload:
 * https://www.elsys.se/en/wp-content/uploads/sites/7/2016/09/Elsys-LoRa-payload_v9.pdf
 *
 * Decode arguments:
 *   - payload: an array of bytes, raw payload from sensor
 *   - port: an integer value in range [0, 255], f_port value from sensor
 */

package main

import (
    "bytes"
    "encoding/binary"
    "github.com/pkg/errors"

    "gitlab.com/smartmakers/drivers/sdk/go/driver/lvl1"
)

const (
    uplinkPort = 5 // Uplinks are sent on the configured port. Device default port value is 5.

    typeTemp = 0x01 // Temperature, size 2 bytes, value from -3276.8°C to 3276.7°C
    typeRH   = 0x02 // Humidity, size 1 byte, value 0-100%
    typeVDD  = 0x07 // Battery, size 2 bytes, value 0-65535mV

    // Elsys ERS-lite device do not have CO2 sensor, but some devices send payloads with CO2 data
    // That data should be ignored
    typeCO2 = 0x06 // CO2, size 2 bytes, value 0-10000ppm
)

type decodedData struct {
    Temperature float64 `json:"temperature"`
    Humidity    int64   `json:"humidity"`
    Battery     int64   `json:"vdd"`
}

func decode(payload []byte, port int) (lvl1.DecodedPayload, error) {
    if port != uplinkPort {
        return nil, errors.Errorf("invalid port: expected %d, got %d", uplinkPort, port)
    }

    data := decodedData{}
    for i := 0; i < len(payload); i++ {
        dataType := payload[i]

        var dataLength int
        switch dataType {
        case typeTemp:
            var dataValue int16
            dataLength = 2
            dataBytes := payload[i+1 : i+1+dataLength]
            err := readBytes(dataBytes, &dataValue)
            if err != nil {
                return nil, errors.Errorf("data type %v has invalid data value: %v", dataType, err)
            }

            data.Temperature = float64(dataValue) / 10
        case typeRH:
            dataLength = 1
            dataValue := int64(payload[i+1])
            data.Humidity = dataValue
        case typeVDD:
            var dataValue uint16
            dataLength = 2
            dataBytes := payload[i+1 : i+1+dataLength]
            err := readBytes(dataBytes, &dataValue)
            if err != nil {
                return nil, errors.Errorf("data type %v has invalid data value: %v", dataType, err)
            }

            data.Battery = int64(dataValue)
        case typeCO2:
            dataLength = 2
        default:
            return nil, errors.Errorf("invalid data type: %v", dataType)
        }

        i += dataLength
    }

    return data, nil
}

func readBytes(b []byte, v interface{}) (err error) {
    buf := bytes.NewReader(b)
    err = binary.Read(buf, binary.BigEndian, v)
    return
}
