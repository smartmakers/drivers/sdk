# Simple Level1 Golang Driver With Schema

This directory contains an example for a lvl1 golang driver that provides a schema.

To build/test/push/delete a driver use `drivers` tool, you may find it: <br/>
  /sdk/bin/linux-amd64/drivers (Linux) <br/>
  /sdk/bin/windows-amd64/drivers (Windows)
   
### Build a driver
To build a driver in the folder with driver run a following command: <br/>
`drivers build` 

### Test a driver
`drivers test ...` //TODO: write a correct command 

### Login the registry repository
`drivers login [driver registry host name] [user name] -p [password]` <br/>
example: `drivers login http://example.com admin -p crypticPassword`

### Push a driver to a registry repository
`drivers push -t [driver tag]` <br/>
example: `drivers push -t 1.0.1 -t 1.0 -t 1`

### Delete a driver from a registry repository
`drivers delete [author/name:tag]` <br/>
example: `drivers delete author/name:1.0.1`
