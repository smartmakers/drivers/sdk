/**
 * Elsys ERS-lite device:
 * https://www.elsys.se/en/wp-content/uploads/sites/7/2016/09/ERS-lite-folder.pdf
 *
 * Official documentation for decoding of payload:
 * https://www.elsys.se/en/wp-content/uploads/sites/7/2016/09/Elsys-LoRa-payload_v9.pdf
 *
 * Decode arguments:
 *   - payload: an array of bytes, raw payload from sensor
 *   - port: an integer value in range [0, 255], f_port value from sensor
 */

UPLINK_PORT = 5;

TYPE_TEMP = 0x01; // Temperature, size 2 bytes, value from -3276.8°C to 3276.7°C
TYPE_RH = 0x02; // Humidity, size 1 byte, value 0-100%
TYPE_VDD = 0x07; // Battery, size 2 bytes, value 0-65535mV

// Elsys ERS-lite device do not have CO2 sensor, but some devices send payloads with CO2 data
// That data should be ignored
TYPE_CO2 = 0x06; // CO2, size 2 bytes, value 0-10000ppm

function decode(payload, port) {
    if (port !== UPLINK_PORT) {
        // there you may add returning of an error message: "invalid port value"
        // but in this example we accept payloads from any port
    }

    var response = new Object();
    for (var i = 0; i < payload.length; i++) {
        var dataType = payload[i];
        var dataLength = 0;
        var dataValue = 0;

        switch (dataType) {
            case TYPE_TEMP: // Temperature
                dataLength = 2; // 2 bytes
                dataValue = bytesToInteger([payload[i + 1], payload[i + 2]]) - 32767;
                response.temperature = dataValue / 10; // -3276.8°C -->3276.7°C
                break;
            case TYPE_RH: // Humidity
                dataLength = 1; // 1 byte
                dataValue = payload[i + 1];
                response.humidity = dataValue; // 0-100%
                break;
            case TYPE_VDD: // Battery level
                dataLength = 1; // 2 bytes
                dataValue = bytesToInteger([payload[i + 1], payload[i + 2]]);
                response.vdd = dataValue; // 0-65535mV
                break;
            case TYPE_CO2: // CO2, should be ignored for ERS-lite device
                dataLength = 2;
                break;
            default: // something is wrong with data, stopping the decoding
                i = payload.length;
        }

        i += dataLength
    }

    return response;
}

function bytesToInteger(array) {
    var value = 0;
    for (var i = 0; i < array.length; i++) {
        value *= 256;
        value += array[i];
    }

    return value;
}
