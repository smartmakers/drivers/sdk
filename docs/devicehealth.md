
# Device Health

Device Health is a feature that allows driver developers to extract device health information from 
the decoded data, like `Battery Status`. This extracted device health information is forwarded to the ThingsHub platform
where it is used for various features. _See [Official ThingsHub Documentation](https://docs.smartmakers.io/thingshub4/)
to see what features are enabled by the device health information._

## Response Format
The Device Health feature is supported by the sdk for drivers starting from the **level 3.2**. It is to be included
as a part of response to an _Uplink Event_ of a driver.

```golang
// A Response is used by a driver to instruct the
// platform on which downlinks to send,
// what to persist until the next request happens,
// and what to send to the application/user.
type Response struct {
	// Downlinks is the list of downlink messages that
	// the driver wants the framework to send to the
	// device.
	//
	// The framework will attempt to send downlinks the
	// order of this array with Downlinks[0] being sent
	// first.
	Downlinks Downlinks `json:"downlinks,omitempty"`
	// The State contains all information that the driver
	// needs to store so that it can access this information
	// during handling of the next event.
	//
	// If the state is nil, it will remain unchanged.
	State interface{} `json:"state,omitempty"`
	// Updates contains time-stamped updates of values
	// measured by the device sent out to the server.
	Updates []Update `json:"updates,omitempty"`
	// DeviceHealth contains time-stamped data decoded
	// from device's uplink related to the device's health
	// in thingshub standard format.
    // Please follow the recommended Response format
    // so that the ThingsHub system can identify these details.
	DeviceHealth []Update `json:"device_health,omitempty"`
}
```
The Device Health entity in the driver response also follows the same format as the **_Updates_**, hence it contains the
following fields:

```golang
type Update struct {
	Time       time.Time   `json:"time"`
	Properties interface{} `json:"properties"`
}
```

The **_ThingsHub_** platform is designed to pick information from the **_Properties_** field of the Device Health Update.
Currently, ThingsHub can pick up the Battery Status information under the **_Battery_** sub-field within the Properties field.

## Format for the Battery Information

For battery status information, the ThingsHub platform traverses all the updates received in the **_DeviceHealth_** structure
and picks the structure that matches the **_Battery_** section in the properties. 
ThingsHub looks for the following specific format and picks out all the available information. 

For the format to work, the `power_source` and either `charge_voltage` or `charge_percentage` fields are must-haves. 
The framework will ignore the data received if these required fields are missing. The framework will also ignore any extra
information coming through the DeviceHealth structure.

```yaml
response:
  device_health:
    - time: "YYYY-MM-DDTHH:MM:SSZ"
      properties:
        battery:
          power_source: battery
          charge_percentage: 86
          charge_voltage: 4.01
          status: discharging
```

### Constraints on Battery Information Format
- `power_source` is a string enum value. It can have one of the following values:
  - `battery`
  - `external-power-supply`
  - `rechargeable-battery`
  - `energy-harvesting`
  - `disposable-battery`
- `charge_percentage` is a float value between and including `0` to `100`.
  - This means: `0 <= charge_percentage <= 100`
  - If `power_source` is set to `external-power-supply`, ThingsHub will automatically set `charge_percentage` to `100`.
- `charge_voltage` is a float value. It represents the voltage measured in Volts (V). 
Logically it is expected to be a positive number. But ThingsHub does not place any specific constraint on it.
- `status` is a string enum value. It can have one of the following values:
  - `charging`
  - `discharging`

## Examples of Responses including Device Health in Yaml Format
### Abeeway Lorawan Devices
```yaml
response:
  state:
    operational_mode:
      mode: "motionTracking"
  
  updates:
    - time: "2021-10-28T04:14:06Z"
      properties:
        battery:
          voltage: 3.77
          status: discharging
        temperature: 26.82
        uplink:
          was_in_motion: true
        event: "geolocationStart"
  
  device_health:
    - time: "2021-10-28T04:14:06Z"
      properties:
        battery:
          power_source: battery
          charge_voltage: 3.77
          status: discharging
```

### Abeeway Lorawan Devices sending multiple data points
```yaml
response:
  state:
    operational_mode:
      mode: "motionTracking"
    ble:
      report_type: "mac"
  updates:
    - time: "2021-10-28T04:14:06Z"
      properties:
        battery:
          status: discharging
          voltage: 4.06
        uplink:
          was_in_motion: true
        temperature: 29.35
        event: bleUplink
        
    - time: "2021-10-28T04:14:06Z"
      properties:
        event: "bleScan"
        beacons:
          c41137406e35: -75
          e420f13dadea: -84
          fe9565567ad8: -87
          
    - time: "2021-10-29T04:14:06Z"
      properties:
        battery:
          status: discharging
          voltage: 4.01
        uplink:
          was_in_motion: true
        temperature: 27.35
        event: bleUplink
        
    - time: "2021-10-29T04:14:06Z"
      properties:
        event: "bleScan"
        beacons:
          c41137406e35: -75
          e420f13dadea: -84
          fe9565567ad8: -87
  
  device_health:
    - time: "2021-10-28T04:14:06Z"
      properties:
        battery:
          power_source: battery
          charge_voltage: 4.06
          status: discharging
          
    - time: "2021-10-29T04:14:06Z"
      properties:
        battery:
          power_source: battery
          charge_voltage: 4.01
          status: discharging
```

### Digitalmatter Cellular Devices
```yaml
response:
  state:
    firmware_version: "1.12"
    iccid: "89314238004000216924"
    imei: "351358810446143"
    product_id: 85
    revision: 1
    serial_number: "417527"

  updates:
    - time: "2021-10-28T04:14:06Z"
      properties:
        internal_battery_voltage: 4540
        internal_temperature: 19
        latitude: 48.9952399
        longitude: 8.3912838
        position_accuracy: 18
        reason: "Start of trip"
        position_source: "LE-Wifi-Premium (Google Wifi)"
        position_source_code: 6

  device_health:
    - time: "2021-10-28T04:14:06Z"
      properties:
        battery:
          power_source: battery
          charge_voltage: 4.54
```