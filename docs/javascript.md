# Javascript Driver Development

Javascript drivers are only supported for compatiblity to decoding scripts
provided by some device manufacturers.

It is not recommended to implement javascript drivers from scratch,
as these are limited in their capabilities and in the achievable end-user experience.

## Creating a Javascript Driver Development Project
Use the following command to initialize a javascript driver development project:

```sh
drivers init javascript
```

## Notes

Javascript drivers do not support
* State management
* Data schemas
* Over-the-air configuration
