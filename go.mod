module gitlab.com/smartmakers/drivers/sdk

go 1.16

require (
	github.com/gopherjs/gopherjs v0.0.0-20191106031601-ce3c9ade29de // indirect
	github.com/pkg/errors v0.9.1
	github.com/robertkrimen/otto v0.0.0-20191219234010-c382bd3c16ff
	github.com/smartystreets/assertions v1.2.1 // indirect
	github.com/smartystreets/goconvey v1.7.2
	github.com/stretchr/testify v1.7.1
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)
