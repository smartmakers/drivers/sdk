# ThingsHub Driver Development Kit

The thingsHub driver software development KIT (Driver SDK) contains pre-built
executables, documentation, and examples for developing and deploying
your own IoT device driver for the thingsHub IoT Integration Plattform.

## Quickstart Guide

Check out the [Quickstart Guide](docs/quickstart.md)
for instructions on how to get started quickly
with developing a basic driver in javascript.

After succesfully finishing the Quickstart Guide,
the next step is to learn more about the concepts
behind device drivers.

The two most important concepts are Driver Capability Levels
and Driver Versioning.
Ẃith this added knowledge,
check out the section about Driver Development below
for more in-depth information on driver development.

## Metadata

Driver metadata is information about a driver
which is not directly part of the driver's executable code itself.
This includes, for example, information about the driver's name, author,
as well as the devices supported by this driver.
See [here](docs/metadata.md) for more information about driver metadata.

## Leveling

ThingsHub allows writing drivers on 3 different levels.
Each level increases the power and flexibility of what the driver can do,
but at the same the complexity of handling the device increases.

Level 1 is for simple decoding uplinks, level 2 adds basic state management,
and level 3 allows event-based processing.

## Driver Schemas

Driver schemas act as a contract between a driver and the system running the driver.
They provide the thingsHub with an understand on how it should interprete a device's data,
and the different features a device provides, even before receiving actual uplinks
from the device.

More information about driver schemas can be found [here](docs/schema.md).

## Device Health
Device Health feature allows the driver to extract and segregate the device status and health information that are 
used to power the device monitoring features in the ThingsHub platform. The Device Health feature is introduced for
drivers in level 3.2 and works with drivers with levels greater than 3.2.

More information on device-health and how to use it can be found [here](docs/devicehealth.md).

## Driver Versioning

Drivers are most useful when they are well-versioned.
See the [Versioning Guide](docs/versioning.md) to understand how versioning works for thingsHub drivers.

## Driver Development with Javascript and Golang

Drivers can currently be developed in [Javascript](docs/javascript.md) or [Golang](docs/golang.md).

## Examples

| Level | Language    | Description                                                                                                           |
|:------|:------------|:----------------------------------------------------------------------------------------------------------------------|
| 1     | javascript  | [Simple javaScript driver](https://gitlab.com/smartmakers/drivers/sdk/tree/master/examples/lvl1/javascript)           |
| 1     | golang      | [Simple golang driver](https://gitlab.com/smartmakers/drivers/sdk/tree/master/examples/lvl1/golang)                   |
| 2     | golang      | [Stateful example with downlinks](https://gitlab.com/smartmakers/drivers/sdk/tree/master/examples/lvl2/golang)        |
| 3     | golang      | [Event-based example](https://gitlab.com/smartmakers/drivers/sdk/tree/master/examples/lvl3/golang)                    |
| 3.1   | golang      | [Action-based example](https://gitlab.com/smartmakers/drivers/sdk/-/tree/master/examples/lvl3.1/golang)               |
| 3.2   | golang      | [Device-health example](https://gitlab.com/smartmakers/drivers/sdk/-/tree/master/examples/lvl3.2/golang)              |
| 3     | javascript  | [Javascript via interpreter example](https://gitlab.com/smartmakers/drivers/sdk/tree/master/examples/lvl3/javascript) |
| 3.1   | javascript  | [Javascript driver with actions example](https://gitlab.com/smartmakers/drivers/sdk/-/tree/master/examples/lvl3.1/javascript)                                                          |
| 3.2   | javascript  | [Javascript driver with device-health example](https://gitlab.com/smartmakers/drivers/sdk/-/tree/master/examples/lvl3.1/javascript)                                                    |

## Contributing

Contributions are welcome and extremely helpful!
Please ensure that you adhere to a commit style
where logically related changes are in a single commit,
or broken up in a way that eases review if necessary.
Keep commit subject lines informative,
but short, and provide additional detail in the extended message text if needed.
If you can, mention relevant issue numbers in either the subject or the extended message.
